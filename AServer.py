from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Util.Padding import pad
from Crypto.Util.Padding import unpad
import socket
import secrets

MY_IP = ""
MY_PORT = 8420
BUFFER_SIZE = 2048

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sc:
  sc.bind((MY_IP, MY_PORT))
  sc.listen(0)
  
  connection, address = sc.accept()
  
  recieve_bytes = connection.recv(BUFFER_SIZE)
  recieve = recieve_bytes.decode("UTF-8")
  print(f"Receive input from {address}: {recieve}")

  send = "Halo Instance B!"
  send_bytes = send.encode("UTF-8")
  connection.send(send_bytes)

  #Dapet key dari A
  receive_key_bytes = connection.recv(BUFFER_SIZE)
  receive_key = receive_key_bytes.decode("UTF-8")
  print(f"Receive PUBLIC KEY from B or {address}: {receive_key}")
  
  #send public key B ke A
  file = open("A_PublicKey.pem","rb")
  public_key = file.read()
  connection.send(public_key)

  file = open("A_PrivateKey.pem","rb")
  private_key_b = file.read()
  import_private_key_b = RSA.import_key(private_key_b)
  cipher_private_rsa = PKCS1_OAEP.new(import_private_key_b)

  #Dapet Symmetric key dari A trs decrypt pake private RSA B
  sym_key = connection.recv(BUFFER_SIZE)
  decrypt = cipher_private_rsa.decrypt(sym_key)
  hex_decrypt = decrypt.hex()
  print(f"Symmetric Key: {hex_decrypt}")

  symmetric_key_bytes = bytes.fromhex(hex_decrypt)
  cipher = AES.new(symmetric_key_bytes, AES.MODE_ECB)

  #Encrypt message dengan symmetric key yg sudah dikirim              
  message = b"Symmetric Key diterima"
  encrypt = cipher.encrypt(pad(message, 32))
  connection.send(encrypt)

  #Nerima pesan 1x
  recieve_next_bytes = connection.recv(BUFFER_SIZE)
  print(f"Pesan sebelum di decrypt: {recieve_next_bytes}")
  decrypt_output = unpad(cipher.decrypt(recieve_next_bytes), 32)
  recieve_next = decrypt_output.decode("UTF-8")
  print(f"Pesan setelah di decrypt: {recieve_next}")
  
  #Kirim pesan 1x
  send_next = b"kenapa gan kasian dehhhh"
  encrypted_send_next = cipher.encrypt(pad(send_next,32))
  connection.send(encrypted_send_next)

  #Dapet pesan stop dari A :(( jahat
  recieve_stop_bytes = connection.recv(BUFFER_SIZE)
  print(f"Pesan sebelum di decrypt: {recieve_stop_bytes}")
  decrypt_recieve_stop_bytes = unpad(cipher.decrypt(recieve_stop_bytes), 32)
  recieve_stop = decrypt_recieve_stop_bytes.decode("UTF-8")
  print(f"Pesan setelah di decrypt: {recieve_stop}")

  #Tutup koneksi
  connection.close()
