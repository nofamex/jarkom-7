from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Util.Padding import pad
from Crypto.Util.Padding import unpad
import socket
import secrets

IP_B = "35.226.48.115"
PORT_B = 6860
BUFFER_SIZE = 2048

sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sc.connect((IP_B, PORT_B))

message = "Halo Instance B!" 
message_bytes = message.encode("UTF-8")
sc.send(message_bytes)

reply_bytes = sc.recv(BUFFER_SIZE)
reply = reply_bytes.decode("UTF-8")
print(f"INSTANCE B SAYS: {reply}\n")  

file = open("A_SymmetricKey.txt","r") 
key_raw = file.read()
            
#Send public key client
file = open("A_PublicKey.pem","rb")
public_key = file.read()
sc.send(public_key)

#got public key B
public_key_b_bytes = sc.recv(BUFFER_SIZE)
public_key_b = RSA.import_key(public_key_b_bytes)
cipher_rsa_b = PKCS1_OAEP.new(public_key_b)
print(f"Public Key B: {public_key_b_bytes}")

#encrypt pake public key b
key_bytes = bytes.fromhex(key_raw)
encrypt_key = cipher_rsa_b.encrypt(key_bytes)
sc.send(encrypt_key)

cipher = AES.new(key_bytes, AES.MODE_ECB)

#decrypt pesan yg dikirim b
output_value_bytes = sc.recv(BUFFER_SIZE)
print(f"Sebelum di decrypt: {output_value_bytes}")
decrypt_output = unpad(cipher.decrypt(output_value_bytes), 32)
decode_decrypt = decrypt_output.decode("UTF-8")
print(f"Sesudah di decrypt: {decode_decrypt}")

#Kirim pesan 1x
next_message_bytes = b"saya suka jarkom tapi...., tapi apa"
encrypt_next_message_bytes = cipher.encrypt(pad(next_message_bytes,32))
sc.send(encrypt_next_message_bytes)

#Dapet pesan 1x
next_response_bytes = sc.recv(BUFFER_SIZE)
print(f"Pesan sebelum di decrypt: {next_response_bytes}")
decrypt_next_response_bytes = unpad(cipher.decrypt(next_response_bytes), 32)
decrypt_next_response = decrypt_next_response_bytes.decode("UTF-8")
print(f"Pesan setelah di decrypt: {decrypt_next_response}")  

#Tutup
stop_message_bytes = b"Stop"
encrypt_stop_message_bytes = cipher.encrypt(pad(stop_message_bytes,32))
sc.send(encrypt_stop_message_bytes)

#Tutup koneksi
sc.close()
