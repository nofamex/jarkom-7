from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
import secrets

#Generate RSA 4096
key_pair = RSA.generate(4096)
public_key = key_pair.public_key()
  
#Write public key to file
file = open("A_PublicKey.pem","wb")
file.write(public_key.export_key())
  
#Write private key to file
file = open("A_PrivateKey.pem","wb")
file.write(key_pair.export_key())

#Generate 32 bytes key for AES-256
key = secrets.token_hex(32)

#Write symmetric key to file
file = open("A_SymmetricKey.txt","w")
file.write(key)


